package hu.webvalto.practices.jpa.stockmarket.daos;

import java.util.List;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
public interface BaseDao {

    <T> List<T> findAll(Class<T> entityClass);

    <T> void save(T entity);

    <T> void delete(T entity);

    <T> void deleteAll(Class<T> entityClass);

}
