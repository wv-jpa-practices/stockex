package hu.webvalto.practices.jpa.stockmarket.daos;

import hu.webvalto.practices.jpa.stockmarket.entities.Broker;
import hu.webvalto.practices.jpa.stockmarket.entities.Company;
import hu.webvalto.practices.jpa.stockmarket.entities.Stock;
import hu.webvalto.practices.jpa.stockmarket.entities.Trade;

import java.util.List;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
public interface TradeDao {

    List<Stock> findStocksForBuying(Broker broker);

    List<Stock> findStocksForSelling(Broker broker);

    List<Trade> findExpiringTrades();

    List<Trade> findSellingTrades();

    List<Trade> findBuyingTradesByPriceAndVolumeLimit(double priceLimit, long volumeLimit, Company company);
}
