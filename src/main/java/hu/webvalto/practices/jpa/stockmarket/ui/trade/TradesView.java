package hu.webvalto.practices.jpa.stockmarket.ui.trade;

import com.vaadin.cdi.CDIView;
import com.vaadin.cdi.UIScoped;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.CssLayout;
import hu.webvalto.practices.jpa.stockmarket.daos.BaseDao;
import hu.webvalto.practices.jpa.stockmarket.entities.Trade;
import org.vaadin.cdiviewmenu.ViewMenuItem;
import org.vaadin.viritin.MSize;
import org.vaadin.viritin.fields.MTable;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;


/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
@UIScoped
@CDIView("Trades")
@ViewMenuItem(order = 0, icon = FontAwesome.EXCHANGE, title = "Trades")
public class TradesView extends CssLayout implements View {

    @Inject
    private BaseDao baseDao;

    private MTable<Trade> tradeTable = new MTable<>(Trade.class)
            .withFullWidth()
            .withHeight("450px")
            .withProperties("stock",
                    "status",
                    "company",
                    "tradeType",
                    "volume",
                    "price",
                    "expires")
            .withColumnHeaders("Stock",
                    "Status",
                    "Company",
                    "TradeType",
                    "Volume",
                    "Price",
                    "Expires");

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        refreshTable();
    }

    @PostConstruct
    private void init() {
        addComponents(
                new MVerticalLayout(
                        new MHorizontalLayout(tradeTable).withFullWidth()
                ).withSize(MSize.FULL_SIZE)
        );
        setSizeFull();
        refreshTable();
    }

    private void refreshTable() {
        tradeTable.setBeans(baseDao.findAll(Trade.class));
    }

}
