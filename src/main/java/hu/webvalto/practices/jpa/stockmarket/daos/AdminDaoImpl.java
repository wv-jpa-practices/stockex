package hu.webvalto.practices.jpa.stockmarket.daos;

import hu.webvalto.practices.jpa.stockmarket.entities.*;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.UUID;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
@Named
public class AdminDaoImpl implements AdminDao {

    @Inject
    private BaseDao baseDao;

    @Override
    public void initialize() {
        createStock();
    }

    @Transactional
    private void createStock() {
        //TODO : validate
        Company company1 = Company.builder().id(UUID.randomUUID().toString()).name("company1").details("company1 details").build();
        Broker broker1 = Broker.builder().id(UUID.randomUUID().toString()).name("broker1").company(company1).build();
        baseDao.save(broker1);
        Company company2 = Company.builder().id(UUID.randomUUID().toString()).name("company2").details("company2 details").build();
        Broker broker2 = Broker.builder().id(UUID.randomUUID().toString()).name("broker2").company(company2).build();
        baseDao.save(broker2);
        Stock stock1 = Stock.builder().id(UUID.randomUUID().toString()).name("stock1").symbol("sym1").price(100d).build();
        baseDao.save(stock1);
        Stock stock2 = Stock.builder().id(UUID.randomUUID().toString()).name("stock2").symbol("sym2").price(100d).build();
        baseDao.save(stock1);
        PortfolioElement portfolioElement1 = PortfolioElement.builder().id(UUID.randomUUID().toString()).volume(10L).company(company1).stock(stock1).build();
        baseDao.save(portfolioElement1);
        PortfolioElement portfolioElement2 = PortfolioElement.builder().id(UUID.randomUUID().toString()).volume(10L).company(company2).stock(stock2).build();
        baseDao.save(portfolioElement2);
    }


    @Override
    public void clear() {
        //TODO : validate
        baseDao.deleteAll(Trade.class);
        baseDao.deleteAll(Broker.class);
        baseDao.deleteAll(PortfolioElement.class);
        baseDao.deleteAll(Stock.class);
        baseDao.deleteAll(Company.class);
    }
}
