package hu.webvalto.practices.jpa.stockmarket.ui;

import com.vaadin.cdi.CDIView;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import hu.webvalto.practices.jpa.stockmarket.daos.AdminDao;
import hu.webvalto.practices.jpa.stockmarket.daos.BaseDao;
import hu.webvalto.practices.jpa.stockmarket.entities.Broker;
import hu.webvalto.practices.jpa.stockmarket.entities.Company;
import hu.webvalto.practices.jpa.stockmarket.ui.trade.TradesView;
import org.vaadin.cdiviewmenu.ViewMenuItem;
import org.vaadin.cdiviewmenu.ViewMenuUI;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
@CDIView("")
@ViewMenuItem(enabled = false)
public class LoginView extends CssLayout implements View {

    @Inject
    private BaseDao baseDao;

    @Inject
    private AdminDao adminDao;

    private Button loginButton;
    private TextField userNameField;
    private PasswordField passwordField;
    private Button initButton;
    private Button clearDataButton;

    public LoginView() {
        setupLoginButton();
        setupFields();

        FormLayout formLayout = new FormLayout(userNameField, passwordField, loginButton);
        formLayout.setSizeUndefined();
        MVerticalLayout mainLayout = new MVerticalLayout(formLayout);
        mainLayout.withFullHeight();
        mainLayout.withFullWidth();
        mainLayout.withAlign(formLayout, Alignment.MIDDLE_CENTER);
        setSizeFull();
        addComponents(mainLayout);
    }

    private void setupFields() {
        userNameField = new TextField("UserName:");
        ShortcutListener shortcut = new ShortcutListener("Login", ShortcutAction.KeyCode.ENTER, null) {
            @Override
            public void handleAction(Object sender, Object target) {
                login();
            }
        };
        userNameField.addShortcutListener(shortcut);
        passwordField = new PasswordField("Password:");
        passwordField.addShortcutListener(shortcut);
    }

    private void setupLoginButton() {
        loginButton = new Button("Login");
        loginButton.addClickListener(clickEvent -> {
            login();
        });
    }

    private void login() {
        String userName = userNameField.getValue();
        String password = passwordField.getValue();
        List<Broker> brokers = baseDao.findAll(Broker.class);
        Predicate<Broker> brokerPredicate = broker -> broker.getName().equals(userName);
        if (!userName.equals("admin") && !brokers.stream().anyMatch(brokerPredicate)) {
            Notification.show("Username not found", Notification.Type.ERROR_MESSAGE);
            return;
        }
        Broker broker = brokers.stream().filter(brokerPredicate).findFirst().orElse(createAdminBroker());
        if (broker.getName().equals("admin")) {
            setupInitDataButton();
            setupClearDataButton();
        }
        ((VaadinUI) VaadinUI.getCurrent()).setLoggedInBroker(broker);
        setupLogout(userName);
        navigateToMain();

    }

    private Broker createAdminBroker() {
        return Broker.builder().name("admin").company(Company.builder().name("adminCompany").portfolio(new ArrayList<>()).build()).build();
    }

    private void setupInitDataButton() {
        initButton = new Button("Initialize data", (Button.ClickListener) event -> initializeEvent());
        if (baseDao.findAll(Broker.class).isEmpty()) {
            initButton.setIcon(FontAwesome.BATTERY_EMPTY);
        } else {
            initButton.setIcon(FontAwesome.BATTERY_FULL);
            initButton.setEnabled(false);
        }
        ViewMenuUI.getMenu().addMenuItem(initButton);
    }

    private void setupClearDataButton() {
        clearDataButton = new Button("Clear data", (Button.ClickListener) event -> clearEvent());
        clearDataButton.setIcon(FontAwesome.REMOVE);
        ViewMenuUI.getMenu().addMenuItem(clearDataButton);
    }

    private void clearEvent() {
        try {
            adminDao.clear();
            initButton.setEnabled(true);
            clearDataButton.setEnabled(false);
            initButton.setIcon(FontAwesome.BATTERY_EMPTY);
        } catch (Exception e) {
            Notification.show(e.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
    }

    private void initializeEvent() {
        try {
            adminDao.initialize();
            initButton.setIcon(FontAwesome.BATTERY_FULL);
            initButton.setEnabled(false);
            clearDataButton.setEnabled(true);
        } catch (Exception e) {
            Notification.show(e.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
    }

    private void setupLogout(String userName) {
        Button logout = new Button("Logout ( " + userName + " )", (Button.ClickListener) event -> logoutEvent());
        logout.setIcon(FontAwesome.SIGN_OUT);
        ViewMenuUI.getMenu().addMenuItem(logout);
    }

    private void logoutEvent() {
        VaadinSession.getCurrent().getSession().invalidate();
        VaadinSession.getCurrent().close();
        Page.getCurrent().setLocation("");
        ((VaadinUI) VaadinUI.getCurrent()).setLoggedInBroker(null);
    }

    private void navigateToMain() {
        ViewMenuUI.getMenu().setVisible(true);
        VaadinUI.getCurrent().getNavigator().navigateTo("Main");
        ViewMenuUI.getMenu().navigateTo(TradesView.class);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        userNameField.focus();
    }
}
