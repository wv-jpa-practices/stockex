package hu.webvalto.practices.jpa.stockmarket.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "TRADES")
public class Trade {

    @Id
    private String id = UUID.randomUUID().toString();

    @Enumerated(EnumType.STRING)
    @Column(name = "TRADE_TYPE")
    private TradeType tradeType;

    private Long volume;

    private Double price;

    @Temporal(TemporalType.TIMESTAMP)
    private Date expires;

    @Enumerated(EnumType.STRING)
    private TradeStatusType status;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "STOCK_ID", referencedColumnName = "ID")
    private Stock stock;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "COMPANY_ID", referencedColumnName = "ID")
    private Company company;

}
