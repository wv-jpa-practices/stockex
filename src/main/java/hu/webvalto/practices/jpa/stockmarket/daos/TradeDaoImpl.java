package hu.webvalto.practices.jpa.stockmarket.daos;

import hu.webvalto.practices.jpa.stockmarket.entities.Broker;
import hu.webvalto.practices.jpa.stockmarket.entities.Company;
import hu.webvalto.practices.jpa.stockmarket.entities.PortfolioElement;
import hu.webvalto.practices.jpa.stockmarket.entities.Stock;
import hu.webvalto.practices.jpa.stockmarket.entities.Trade;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
@Named
public class TradeDaoImpl implements TradeDao {

    @Inject
    private BaseDao baseDao;

    @PersistenceContext(unitName = "stockPU")
    private EntityManager entityManager;

    @Override
    public List<Stock> findStocksForBuying(Broker broker) {
        return baseDao.findAll(Stock.class);
    }

    @Override
    public List<Stock> findStocksForSelling(Broker broker) {
        return broker.getCompany().getPortfolio().stream()
                .filter(portfolioElement -> portfolioElement.getVolume() > 0)
                .map(PortfolioElement::getStock)
                .collect(Collectors.toList());
    }

    @Override
    public List<Trade> findExpiringTrades() {
        TypedQuery<Trade> expiringTradesQuery = entityManager.createQuery("select t from Trade t where t.expires <= :checkDate " +
                "and t.status = hu.webvalto.practices.jpa.stockmarket.entities.TradeStatusType.ACTIVE", Trade.class);
        expiringTradesQuery.setParameter("checkDate", new Date());
        return expiringTradesQuery.getResultList();
    }

    @Override
    public List<Trade> findSellingTrades() {
        TypedQuery<Trade> sellingTradesQuery = entityManager.createQuery("select t from Trade t " +
                "where t.tradeType = hu.webvalto.practices.jpa.stockmarket.entities.TradeType.SELL " +
                "and t.status = hu.webvalto.practices.jpa.stockmarket.entities.TradeStatusType.ACTIVE ", Trade.class);
        return sellingTradesQuery.getResultList();
    }

    @Override
    public List<Trade> findBuyingTradesByPriceAndVolumeLimit(double priceLimit, long volumeLimit, Company company) {
        TypedQuery<Trade> tradeTypedQuery = entityManager.createQuery("select t from Trade t " +
                        "where t.price >= :priceLimit and t.volume <= :volumeLimit " +
                        "and t.tradeType = hu.webvalto.practices.jpa.stockmarket.entities.TradeType.BUY " +
                        "and t.status = hu.webvalto.practices.jpa.stockmarket.entities.TradeStatusType.ACTIVE " +
                        "and t.company <> :company " +
                        "order by t.volume, t.price",
                Trade.class);
        tradeTypedQuery.setParameter("priceLimit", priceLimit);
        tradeTypedQuery.setParameter("volumeLimit", volumeLimit);
        tradeTypedQuery.setParameter("company", company);
        return tradeTypedQuery.getResultList();
    }
}
