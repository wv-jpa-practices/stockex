package hu.webvalto.practices.jpa.stockmarket.services;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static java.time.temporal.ChronoUnit.MINUTES;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
public interface SellService extends TradeService {
}
