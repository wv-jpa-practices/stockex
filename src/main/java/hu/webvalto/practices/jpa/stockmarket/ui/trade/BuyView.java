package hu.webvalto.practices.jpa.stockmarket.ui.trade;

import com.vaadin.cdi.CDIView;
import com.vaadin.cdi.UIScoped;
import com.vaadin.server.FontAwesome;
import hu.webvalto.practices.jpa.stockmarket.entities.Stock;
import hu.webvalto.practices.jpa.stockmarket.services.BuyService;
import org.vaadin.cdiviewmenu.ViewMenuItem;

import javax.inject.Inject;
import java.util.List;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
@UIScoped
@CDIView("Buy")
@ViewMenuItem(order = 4, icon = FontAwesome.ARROW_LEFT, title = "Buy")
public class BuyView extends TradeViewDefinition {

    @Inject
    private BuyService buyService;

    @Override
    protected void makeTrade() {
        buyService.makeTrade(getVolumeValue(), getPriceValue(), getSelectedStock());
    }

    @Override
    protected List<Stock> getStocks() {
        return tradeDaoService.findStocksForBuying(getLoggedInBroker());
    }
}
