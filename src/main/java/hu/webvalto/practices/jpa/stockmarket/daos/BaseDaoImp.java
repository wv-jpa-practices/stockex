package hu.webvalto.practices.jpa.stockmarket.daos;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@Named
@Transactional
public class BaseDaoImp implements BaseDao {

    @PersistenceContext(unitName = "stockPU")
    private EntityManager entityManager;

    @Override
    public <T> List<T> findAll(Class<T> entityClass) {
        String entityName = entityManager.getMetamodel().entity(entityClass).getName();
        TypedQuery<T> typedQuery = entityManager.createQuery("select e from " + entityName + " e", entityClass);
        return typedQuery.getResultList();
    }

    @Override
    public <T> void save(T entity) {
        entityManager.merge(entity);
    }

    @Override
    public <T> void delete(T entity) {
        entityManager.remove(entity);
    }

    @Override
    public <T> void deleteAll(Class<T> entityClass) {
        findAll(entityClass).forEach(entity -> entityManager.remove(entity));
    }

}
