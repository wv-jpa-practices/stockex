package hu.webvalto.practices.jpa.stockmarket.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.cdi.CDIUI;
import com.vaadin.server.VaadinRequest;
import hu.webvalto.practices.jpa.stockmarket.daos.BaseDao;
import hu.webvalto.practices.jpa.stockmarket.entities.Broker;
import lombok.Setter;
import org.vaadin.cdiviewmenu.ViewMenuUI;

import javax.inject.Inject;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@CDIUI("")
@Theme("valo")
@Title("StockMarket")
public class VaadinUI extends ViewMenuUI {

    @Setter
    private Broker loggedInBroker;

    @Inject
    private BaseDao baseDao;

    @Override
    protected void init(VaadinRequest request) {
        super.init(request);
        ViewMenuUI.getMenu().setVisible(false);
        ViewMenuUI.getMenu().navigateTo("");
    }

    public Broker getLoggedInBroker() {
        if (loggedInBroker != null) {
            return baseDao.findAll(Broker.class).stream().filter(broker1 -> broker1.getId().equals(loggedInBroker.getId())).findFirst().orElse(loggedInBroker);
        }
        return null;
    }
}
