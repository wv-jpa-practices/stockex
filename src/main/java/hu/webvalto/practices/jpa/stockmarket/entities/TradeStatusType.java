package hu.webvalto.practices.jpa.stockmarket.entities;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
public enum TradeStatusType {

    ACTIVE,
    CLOSED,
    EXPIRED,
    CANCELLED

}
